<?php

/**
 * @file
 * This file contains all of the forms for the module.
 */

/**
 * socialflow settings form callback
 */
function socialflow_settings_form() {

  $socialflow_entities = variable_get('socialflow_entities', array());

  $form = array();

  $socialflow_consumer_key = variable_get('socialflow_consumer_key', '');
  $socialflow_consumer_secret = variable_get('socialflow_consumer_secret', '');

  socialflow_validate_current_authorization();

  if (!empty($socialflow_consumer_key) && !empty($socialflow_consumer_secret)) {
    $status_table = array(
      '#theme' => 'table',
      '#header' => array(t('SocialFlow OAuth Key'), t('SocialFlow OAuth Key Secret'), t('Actions')),
      '#empty' => t('There is no content in this queue.'),
      '#rows' => array(),
    );

    $oauth_token = variable_get('socialflow_oauth_token', '');
    $oauth_token_secret = variable_get('socialflow_oauth_token_secret', '');

    if (empty($oauth_token) && empty($oauth_token_secret)) {
      socialflow_prompt_user_to_login();
    }

    $accounts_table = '';
    if (!empty($oauth_token) && !empty($oauth_token_secret)) {

      $status_table['#rows'][] = array($oauth_token, $oauth_token_secret, l(t('Disconnect'), 'socialflow/disconnect'));

      $form['socialflow_status'] = array(
        '#markup' => render($status_table),
      );
    }
  }

  $logo_path = drupal_get_path('module', 'socialflow') . '/socialflow_logo_bw.jpg';
  $form['post-form']['socialflow-logo'] = array(
    '#markup' => '<div class="socialflow-logo"><img src="/' . $logo_path . '" /></div>',
  );

  $form['bundle_markup'] = array(
    '#markup' => t('Select the content types to use with SocialFlow.'),
  );

  // Construct options array of entity types.
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if ($entity_type == 'node') {
      $options = array();
      foreach ($entity_info['bundles'] as $key => $info) {
        $options[$key] = $info['label'];
      }

      $form['socialflow_entities']['#tree'] = TRUE;

      $form['socialflow_entities'][$entity_type] = array(
        '#type' => 'checkboxes',
        '#title' => check_plain($entity_info['label']),
        '#options' => $options,
        '#default_value' => $socialflow_entities && array_key_exists($entity_type, $socialflow_entities) ? $socialflow_entities[$entity_type] : array(),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * socialflow_settings_form_submit
 */
function socialflow_settings_form_submit(&$form, &$form_state) {

  variable_set('socialflow_entities', $form_state['values']['socialflow_entities']);

  $oauth_token = variable_get('socialflow_oauth_token', '');
  $oauth_token_secret = variable_get('socialflow_oauth_token_secret', '');

  if (empty($oauth_token) && empty($oauth_token_secret)) {
    drupal_goto('admin/config/socialflow/sign_in');
  }

  drupal_set_message(t('SocialFlow settings saved.'));
}

/**
 * socialflow_reset_accounts_form
 */
function socialflow_reset_accounts_form($form, &$form_state) {
  $form = array();

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Refresh Accounts',
  );

  return $form;
}

/**
 * socialflow_reset_accounts_form_submit
 */
function socialflow_reset_accounts_form_submit($form, &$form_state) {
  socialflow_reset_accounts();
  drupal_set_message(t('SocialFlow accounts refreshed.'));
}

/**
 * socialflow_reset_accounts_form_submit
 */
function socialflow_navigate_accounts_form($form, &$form_state) {
  $form = array();
  drupal_add_js(drupal_get_path('module', 'socialflow') . '/socialflow.js', 'file');
  $options = array();
  $accounts = variable_get('socialflow_accounts', '');

  foreach ($accounts as $account_index => $account) {
    if ($account->service_type == 'publishing') {
      $property_name = socialflow_get_account_property_name($account);
      $url = 'content_queue/' . $account->service_user_id . '/' . $account->account_type . '/';
      $options[$url] = drupal_ucfirst($account->account_type) . ' : ' . $account->name;
    }
  }

  drupal_add_js('jQuery(document).ready(function(){SOCIALFLOW.contentQueueNavigation.init();});', 'inline');

  $account_id = arg(4);
  $account_type = arg(5);

  $form['socialflow-accounts'] = array(
    '#type' => 'select',
    '#title' => t('Accounts'),
    '#options' => $options,
    '#description' => t('Choose an account to see its content queue.'),
    '#default_value' => 'content_queue/' . $account_id . '/' . $account_type . '/',
  );

  return $form;
}

/**
 * socialflow_add_content_channel_form
 */
function socialflow_add_content_channel_form() {
  $form = array();

  $form['add-content-channel-form'] = array(
    '#type' => 'fieldset',
    '#title' => 'Add Content Channel',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['add-content-channel-form']['content-source'] = array(
    '#type' => 'select',
    '#title' => t('Content Source'),
    '#options' => array('RSS' => 'RSS', '3' => 'XML'),
    '#default_value' => array('rss'),
  );

  $form['add-content-channel-form']['content-channel-name'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => 'Channel Name',
  );

  $form['add-content-channel-form']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

/**
 * socialflow_add_content_channel_form_submit
 */
function socialflow_add_content_channel_form_submit($form, &$form_state) {
  $api_key = variable_get('socialflow_api_key');
  $clients = variable_get('socialflow_clients', array());
  $values = $form_state['values'];
  $name = filter_xss($values['content-channel-name']);
  $socialflow = new Socialflow($api_key);
  $response = $socialflow->addContentChannel($clients[0]->id, $name, $values['content-source']);
  // For the socialflow api call per second limit.
  sleep(.5);
  if ($response->status == 400) {
    drupal_set_message($response->data->message, 'error');
  }
  else {
    drupal_set_message(check_plain('"' . $name . '" Content Channel added with source ' . $values['content-source']));
  }

}

/**
 * socialflow_add_feed_form
 */
function socialflow_add_feed_form($form, &$form_state, $accounts = array()) {

  $content = '';

  $form = array();

  $form['add-feed-form'] = array(
    '#type' => 'fieldset',
    '#title' => 'Add content source',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 10,
  );

  $form['add-feed-form']['feed-url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#required' => TRUE,
    '#description' => 'RSS or Atom feed URL',
  );

  $form['add-feed-form']['feed-title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => 'Feed title',
  );

  $options = array();

  if (!empty($accounts)) {
    foreach ($accounts as $account) {
      $options[$account->service_user_id] = drupal_ucfirst($account->account_type) . ' : ' . $account->name;
    }
  }

  $form['add-feed-form']['account'] = array(
    '#type' => 'select',
    '#title' => t('Which social account should this content feed to?'),
    '#options' => $options,
  );

  $form['add-feed-form']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'submit',
  );

  return $form;
}

/**
 * socialflow_add_feed_form_validate
 */
function socialflow_add_feed_form_validate($form, &$form_state) {
  // TODO: validate format of URL using preg_match.
}

/**
 * socialflow_add_feed_form_submit
 */
function socialflow_add_feed_form_submit($form, &$form_state) {

  $accounts = variable_get('socialflow_accounts', array());
  $values = $form_state['values'];

  $feed_url = $values['feed-url'];
  $feed_title = $values['feed-title'];
  $service_user_id = $values['account'];
  $account_type = '';

  foreach ($accounts as $account) {
    if ($account->service_user_id == $service_user_id) {
      $account_type = $account->account_type;
    }
  }

  $socialflow_oauth = socialflow_build_oauth_object();

  $response = $socialflow_oauth->addFeed($service_user_id, $account_type, $feed_url, $feed_title);

  if ($response->success) {
    drupal_set_message(t('Content source successfully added'));
  }
}

/**
 * socialflow_publish_message_form
 */
function socialflow_publish_message_form($form, &$form_state, $service_user_id, $content_item_id, $content_message) {
  $accounts = variable_get('socialflow_accounts', array());

  $form = array();

  $form['referer'] = array(
    '#type' => 'hidden',
    '#default_value' => $_SERVER['HTTP_REFERER'],
  );

  $form['service-user-id'] = array(
    '#type' => 'hidden',
    '#default_value' => $service_user_id,
  );

  $form['content-item-id'] = array(
    '#type' => 'hidden',
    '#default_value' => $content_item_id,
  );

  $form['content-message'] = array(
    '#type' => 'hidden',
    '#default_value' => $content_message,
  );

  $question = t('Are you sure you want to immediately publish this message "' . urldecode($content_message) . '"?');
  return confirm_form($form, $question, $_SERVER['HTTP_REFERER'], 'Immediately publish this message?', 'Publish now.');
}

/**
 * socialflow_publish_message_form
 */
function socialflow_publish_message_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $service_user_id = $values['service-user-id'];
  $content_item_id = $values['content-item-id'];
  $content_message = $values['content-message'];

  $accounts = variable_get('socialflow_accounts');
  $account_type = '';
  foreach ($accounts as $account) {
    if ($account->service_user_id == $service_user_id) {
      $account_type = $account->account_type;
    }
  }

  $socialflow_oauth = socialflow_build_oauth_object();

  $response = $socialflow_oauth->editMessage($service_user_id, $content_item_id, $account_type, $content_message, 'publish now');

  if (property_exists($response, 'success')) {
    if ($response->success) {
      drupal_set_message(t('Message published.'));
    }
  }
  else {
    drupal_set_message(t('Error publishing message.'), 'error');
  }

  drupal_goto($values['referer']);
}

/**
 * socialflow_delete_message_form
 */
function socialflow_delete_message_form($form, &$form_state, $service_user_id, $content_item_id, $content_message) {

  $form = array();

  $form['referer'] = array(
    '#type' => 'hidden',
    '#default_value' => $_SERVER['HTTP_REFERER'],
  );

  $form['service-user-id'] = array(
    '#type' => 'hidden',
    '#default_value' => $service_user_id,
  );

  $form['content-item-id'] = array(
    '#type' => 'hidden',
    '#default_value' => $content_item_id,
  );

  $question = t('Are you sure you want to delete this message "' . urldecode($content_message) . '"?');
  return confirm_form($form, $question, $_SERVER['HTTP_REFERER'], 'This action cannot be undone.', 'Delete');
}

/**
 * socialflow_delete_message_form_submit
 */
function socialflow_delete_message_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $service_user_id = $values['service-user-id'];
  $content_item_id = $values['content-item-id'];

  $accounts = variable_get('socialflow_accounts');
  $account_type = '';
  foreach ($accounts as $account) {
    if ($account->service_user_id == $service_user_id) {
      $account_type = $account->account_type;
    }
  }

  $socialflow_oauth = socialflow_build_oauth_object();

  $response = $socialflow_oauth->deleteMessageFromQueue($content_item_id, $service_user_id, $account_type);
  if (property_exists($response, 'success')) {
    if ($response->success) {
      drupal_set_message(t('Message deleted.'));
    }
  }
  else {
    drupal_set_message(t('Error deleting message.'), 'error');
  }

  drupal_goto($values['referer']);
}

/**
 * socialflow_quickpost_form
 */
function socialflow_quickpost_form() {

  drupal_add_js(drupal_get_path('module', 'socialflow') . '/socialflow.js', 'file');
  // Add JS for datepicker helper.
  drupal_add_js('SOCIALFLOW.datePickerHelper.init();', 'inline');

  $form = array();

  $form['#attributes'] = array('class' => array('socialflow-post-form'));

  $accounts = variable_get('socialflow_accounts', array());

  $accounts_options = array();

  foreach ($accounts as $account) {
    $name = !empty($account->screen_name) ? $account->screen_name : $account->name;
    $accounts_options[$account->service_user_id . ':' . $account->account_type] = drupal_ucfirst($account->account_type) . ' : ' . $name;
  }

  $default_account_value = $accounts[0]->service_user_id . ':' . $accounts[0]->account_type;

  $logo_path = drupal_get_path('module', 'socialflow') . '/socialflow_logo_bw.jpg';
  $form['socialflow-logo'] = array(
    '#markup' => '<div class="socialflow-logo"><img src="/' . $logo_path . '" /></div>',
  );

  $form['account'] = array(
    '#type' => 'select',
    '#title' => t('Social Account'),
    '#description' => t('Select one or more social accounts.'),
    '#options' => $accounts_options,
    '#multiple' => TRUE,
    '#required' => TRUE,
    '#default_value' => array($default_account_value),
  );

  $form['shorten-links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Shorten Links'),
    '#default_value' => TRUE,
  );

  $form['message'] = array(
    '#prefix' => '<div class="socialflow-character-limit">140</div>',
    '#type' => 'textarea',
    '#title' => 'message',
    '#rows' => 3,
    '#required' => TRUE,
  );

  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#options' => array('optimize' => 'Optimize', 'publish' => 'Publish', 'schedule' => 'Schedule', 'hold' => 'Hold'),
    '#default_value' => 'optimize',
  );

  $form['date-and-time'] = array(
    '#type' => 'date_popup',
    '#title' => 'Optimize from',
    '#default_value' => '',
    '#date_format' => 'm/d/Y h:i a',
  );

  $form['end-date'] = array(
    '#type' => 'date_popup',
    '#title' => 'Optimize to',
    '#default_value' => '',
    '#date_format' => 'm/d/Y h:i a',
  );

  $form['must-send'] = array(
    '#type' => 'checkbox',
    '#title' => t('Must Send'),
    '#default_value' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Send Content',
    '#disabled' => (!empty($accounts) ? FALSE : TRUE),
  );

  return $form;
}

/**
 * socialflow_quickpost_form_validate
 */
function socialflow_quickpost_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  switch ($values['status']) {
    case 'schedule':
      if (empty($values['date-and-time'])) {
        form_set_error('date-and-time', t('You must specify a date when scheduling content.'));
        drupal_add_js('SOCIALFLOW.validationHelper.init("schedule");', 'inline');
      }
      else {
        $selected_time = strtotime($values['date-and-time']);
        $now = time();
        if ($selected_time < $now) {
          form_set_error('date-and-time', t('You can only schedule content in the future. Please select a valid date.'));
          drupal_add_js('SOCIALFLOW.validationHelper.init("schedule");', 'inline');
        }
      }
      break;

    case 'optimize':
      if (empty($values['date-and-time']) && !empty($values['end-date'])) {
        form_set_error('date-and-time', t('To optimize within a time window you must select a start date and an end date.'));
      }
      if (!empty($values['date-and-time']) && empty($values['end-date'])) {
        form_set_error('end-date', t('To optimize within a time window you must select a start date and an end date.'));
      }
      break;

    case 'hold':
      if (empty($values['date-and-time'])) {
        form_set_error('date-and-time', t('You must specify an expiration date when holding content.'));
        drupal_add_js('SOCIALFLOW.validationHelper.init("hold");', 'inline');
      }
      break;
  }
}

/**
 * socialflow_quickpost_form_submit
 */
function socialflow_quickpost_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $socialflow_date = NULL;
  $socialflow_end_date = NULL;
  if (!empty($values['date-and-time'])) {
    $date_and_time = strtotime($values['date-and-time']);
    $socialflow_date = date('d/M/Y:G:i:s', $date_and_time);

    $socialflow_end_date = NULL;
    if (!empty($values['end-date'])) {
      $raw_end_date = strtotime($values['end-date']);
      $socialflow_end_date = date('d/M/Y:G:i:s', $raw_end_date);
    }
  }

  $message = $values['message'];

  $selected_accounts = $values['account'];
  $total_sent_to_socialflow = 0;
  foreach ($selected_accounts as $account) {
    $account_array = explode(':', $account);
    $service_user_id = $account_array[0];
    $account_type = $account_array[1];

    if ($values['shorten-links']) {
      $shorten_links = $values['shorten-links'];
    }
    else {
      $shorten_links = 0;
    }

    $must_send = 0;
    if (!empty($values['must-send'])) {
      $must_send = 1;
    }

    $socialflow_oauth = socialflow_build_oauth_object();
    $response;
    switch ($values['status']) {
      case 'optimize':
        $response = $socialflow_oauth->addMessageToQueue($service_user_id, $account_type, $message, 'optimize', $socialflow_date, $socialflow_end_date, $must_send, NULL, $shorten_links);
        break;

      case 'publish':
        $response = $socialflow_oauth->addMessageToQueue($service_user_id, $account_type, $message, 'publish now', NULL, NULL, 0, NULL, $shorten_links);
        break;

      case 'schedule':
        $response = $socialflow_oauth->addMessageToQueue($service_user_id, $account_type, $message, 'schedule', $socialflow_date, NULL, 0, NULL, $shorten_links);
        break;

      case 'hold':
        $response = $socialflow_oauth->addMessageToQueue($service_user_id, $account_type, $message, 'hold', NULL, NULL, 0, $socialflow_date, $shorten_links);
        break;
    }

    if (!$response->success) {
      foreach ($response->errors as $error_field => $error) {
        foreach ($error as $error_field_error) {
          drupal_set_message($error_field . ' : ' . $error_field_error['msgid'] . ' : ' . $error_field_error['message'], 'error');
        }
      }
    }
    else {
      // Sleep 600 milliseconds so not to go over SF's rate limit.
      sleep(0.6);
      $total_sent_to_socialflow++;
    }
  } // End foreach for $selected_accounts.
  if ($total_sent_to_socialflow == 1) {
    drupal_set_message(t('Message sent to SocialFlow.'));
  }
  else {
    drupal_set_message($total_sent_to_socialflow . ' messages sent to SocialFlow.');
  }
}

/**
 * socialflow_batch_content_form
 */
function socialflow_batch_content_form($form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'socialflow') . '/socialflow.js', 'file');
  // Add JS for datepicker helper.
  drupal_add_js('SOCIALFLOW.datePickerHelper.init();', 'inline');

  // Build the sortable table header.
  $header = array(
      'entity_type' => array('data' => t('Entity Type'), 'field' => 'entity_type'),
      'bundle' => array('data' => t('Bundle'), 'field' => 'bundle'),
      'title' => array('data' => t('Title'), 'field' => 'title'),
      'entity_id' => array('data' => t('Entity ID'), 'field' => 'entity_id'),
  );

  $query = new EntityFieldQuery();
  $entity_types = '';
  $options = array();

  $socialflow_entities = variable_get('socialflow_entities', array());
  foreach ($socialflow_entities as $entity_type_name => $entity_type) {
    if ($entity_type_name == 'node' | $entity_type_name == 'user') {
      foreach ($entity_type as $bundle_name => $bundle) {
        if ($bundle) {
          $entity_info = entity_get_info($entity_type_name);
          $id_name = $entity_info['entity keys']['id'];
          $entity_result = $query->entityCondition('entity_type', $entity_type_name, '=')
                            ->entityCondition('bundle', $bundle_name, '=')->execute();
          if (!empty($entity_result[$entity_type_name])) {
            foreach ($entity_result[$entity_type_name] as $entity) {
              $full_entity = entity_load($entity_type_name, array($entity->$id_name));
              $full_entity = $full_entity[$entity->$id_name];
              $entity_title = '';
              if (property_exists($full_entity, 'title')) {
                $entity_title = $full_entity->title;
              }
              elseif (property_exists($full_entity, 'name')) {
                $entity_title = $full_entity->name;
              }

              if ($entity_type_name == 'user') {
                if (!empty($entity->$id_name) && $entity->$id_name != 1) {
                  $options[$entity_type_name . '-' . $bundle_name . '-' . $entity->$id_name] = array(
                    'entity_type' => t($entity_type_name),
                    'bundle' => t($bundle_name),
                    'title' => t($entity_title),
                    'entity_id' => t($entity->$id_name),
                  );
                }
              }
              else {
                $options[$entity_type_name . '-' . $bundle_name . '-' . $entity->$id_name] = array(
                  'entity_type' => t($entity_type_name),
                  'bundle' => t($bundle_name),
                  'title' => t($entity_title),
                  'entity_id' => t($entity->$id_name),
                );
              }
            }
          }
        }
      }
    }
  }

  $form = array();

  $form['#attributes'] = array('class' => array('socialflow-batch-post-form'));

  $form['post-form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Post Settings'),
  );

  $accounts = variable_get('socialflow_accounts', array());

  $accounts_options = array();

  foreach ($accounts as $account) {
    $name = !empty($account->screen_name) ? $account->screen_name : $account->name;
    $accounts_options[$account->service_user_id . ':' . $account->account_type] = drupal_ucfirst($account->account_type) . ' : ' . $name;
  }

  if (empty($accounts)) {
    socialflow_prompt_user_to_login();
    return NULL;
  }

  $default_account_value = $accounts[0]->service_user_id . ':' . $accounts[0]->account_type;

  $logo_path = drupal_get_path('module', 'socialflow') . '/socialflow_logo_bw.jpg';
  $form['post-form']['socialflow-logo'] = array(
    '#markup' => '<div class="socialflow-logo"><img src="/' . $logo_path . '" /></div>',
  );

  $form['post-form']['account'] = array(
    '#type' => 'select',
    '#title' => t('Social Account'),
    '#options' => $accounts_options,
    '#multiple' => TRUE,
    '#required' => TRUE,
    '#default_value' => array($default_account_value),
  );

  $form['post-form']['shorten-links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Shorten links'),
    '#default_value' => TRUE,
  );

  $form['post-form']['message'] = array(
    '#prefix' => '<div class="socialflow-character-limit">118</div>',
    '#type' => 'textarea',
    '#title' => 'message',
    '#description' => t("A message to attach to every post. If left blank each message will be the entity's title and link."),
    '#rows' => 3,
  );

  $form['post-form']['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#options' => array('optimize' => 'Optimize', 'publish' => 'Publish Now', 'schedule' => 'Schedule', 'hold' => 'Hold'),
    '#default_value' => 'optimize',
  );

  $form['post-form']['date-and-time'] = array(
    '#type' => 'date_popup',
    '#title' => 'Optimize from',
    '#default_value' => '',
    '#date_format' => 'm/d/Y h:i a',
  );

  $form['post-form']['end-date'] = array(
    '#type' => 'date_popup',
    '#title' => 'Optimize to',
    '#default_value' => '',
    '#date_format' => 'm/d/Y h:i a',
  );

  $form['post-form']['must-send'] = array(
    '#type' => 'checkbox',
    '#title' => t('Must Send'),
    '#default_value' => FALSE,
  );

  $form['entities'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#attributes' => array('class' => array('entity-sort-table')),
    '#empty' => t('No content available.'),
  );

  $form['pager'] = array('#markup' => theme('pager'));

  $submit_enabled = socialflow_validate_current_authorization();

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send Selected'),
    '#disabled' => !$submit_enabled,
  );

  return $form;

}

/**
 * socialflow_batch_content_form_validate
 */
function socialflow_batch_content_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  switch ($values['status']) {
    case 'schedule':
      if (empty($values['date-and-time'])) {
        form_set_error('date-and-time', t('You must specify a date when scheduling content.'));
        drupal_add_js('SOCIALFLOW.validationHelper.init("schedule");', 'inline');
      }
      else {
        $selected_time = strtotime($values['date-and-time']);
        $now = time();
        if ($selected_time < $now) {
          form_set_error('date-and-time', t('You can only schedule content in the future. Please select a valid date.'));
        }
      }
      break;

    case 'optimize':
      if (empty($values['date-and-time']) && !empty($values['end-date'])) {
        form_set_error('date-and-time', t('To optimize within a time window you must select a start date and an end date.'));
      }
      if (!empty($values['date-and-time']) && empty($values['end-date'])) {
        form_set_error('end-date', t('To optimize within a time window you must select a start date and an end date.'));
      }
      break;

    case 'hold':
      if (empty($values['date-and-time'])) {
        form_set_error('date-and-time', t('You must specify an expiration date when holding content.'));
        drupal_add_js('SOCIALFLOW.validationHelper.init("hold");', 'inline');
      }
      break;
  }

  $selected = 0;
  foreach ($values['entities'] as $entity) {
    if (gettype($entity) == 'string') {
      $selected++;
    }
  }

  if ($selected == 0) {
    form_set_error('entities', t('You must select at least one piece of content to send to SocialFlow.'));
  }

}

/**
 * socialflow_batch_content_form_submit
 */
function socialflow_batch_content_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $entities = $values['entities'];
  $full_entities = array();
  $post_options = array();
  if (!empty($values['message'])) {
    $post_options['message'] = filter_xss($values['message']);
  }

  $post_options['status'] = $values['status'];
  $post_options['must-send'] = $values['must-send'];
  $post_options['date-and-time'] = $values['date-and-time'];
  $post_options['end-date'] = $values['end-date'];
  $post_options['account'] = $values['account'];
  $post_options['shorten-links'] = $values['shorten-links'];

  foreach ($entities as $entity_string) {
    if (!empty($entity_string)) {
      $entity_array = explode('-', $entity_string);
      $entity_type = $entity_array[0];
      $bundle = $entity_array[1];
      $entity_id = $entity_array[2];

      $entity = entity_load($entity_type, array($entity_id));

      $full_entities[] = $entity[$entity_id];
    }
  }

  $batch = array(
    'operations' => array(
      array('socialflow_batch_process_entity', array($full_entities, $post_options)),
    ),
    'finished' => 'socialflow_batch_process_finished',
    'title' => t('Sending content to SocialFlow'),
    'init_message' => t('Initializing...'),
    'progress_message' => t('Completed @current batch process out of @total.'),
    'error_message' => t('SocialFlow batch process has encountered an error.'),
  );

  batch_set($batch);

  batch_process('admin/config/socialflow/batch');

}

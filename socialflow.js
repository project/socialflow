var SOCIALFLOW = { };

(function($){
  SOCIALFLOW.postFormHelper = {
    init:function() {
      $('.socialflow-post-form #edit-status div.form-item input').click(function() {
        switch ($(this).attr('value')) {
          case 'optimize':
            $('.socialflow-post-form .form-item-must-send').show();
            $('.socialflow-post-form .form-item-date-and-time').show();
            $('.socialflow-post-form .form-item-end-date').show();
            $('.socialflow-post-form .form-item-date-and-time label[for="edit-date-and-time"]').text('Optimize from');
          break;

          case 'publish':
            $('.socialflow-post-form .form-item-must-send').hide();
            $('.socialflow-post-form .form-item-date-and-time').hide();
            $('.socialflow-post-form .form-item-end-date').hide();
          break;

          case 'schedule':
            $('.socialflow-post-form .form-item-must-send').hide();
            $('.socialflow-post-form .form-item-date-and-time').show();
            $('.socialflow-post-form .form-item-end-date').hide();
            $('.socialflow-post-form .form-item-date-and-time label[for="edit-date-and-time"]').text('Send at');
          break;

          case 'hold':
            $('.socialflow-post-form .form-item-must-send').hide();
            $('.socialflow-post-form .form-item-date-and-time').show();
            $('.socialflow-post-form .form-item-end-date').hide();
            $('.socialflow-post-form .form-item-date-and-time label[for="edit-date-and-time"]').text('Expires at');
          break;
        }
      });

      $('.node-form .form-item-socialflow-status .form-item input').click(function() {
        switch ($(this).attr('value')) {
          case 'optimize':
            $('.node-form .form-item-must-send').show();
            $('.node-form .form-item-date-and-time').show();
            $('.node-form .form-item-end-date').show();
            $('.node-form .form-item-date-and-time label[for="edit-date-and-time"]').text('Optimize from');
          break;

          case 'publish':
            $('.node-form .form-item-must-send').hide();
            $('.node-form .form-item-date-and-time').hide();
            $('.node-form .form-item-end-date').hide();
          break;

          case 'schedule':
            $('.node-form .form-item-must-send').hide();
            $('.node-form .form-item-date-and-time').show();
            $('.node-form .form-item-end-date').hide();
            $('.node-form .form-item-date-and-time label[for="edit-date-and-time"]').text('Send at');
          break;

          case 'hold':
            $('.node-form .form-item-must-send').hide();
            $('.node-form .form-item-date-and-time').show();
            $('.node-form .form-item-end-date').hide();
            $('.node-form .form-item-date-and-time label[for="edit-date-and-time"]').text('Expires at');
          break;
        }
      });

      $('.socialflow-batch-post-form #edit-status div.form-item input').click(function() {
        switch ($(this).attr('value')) {
          case 'optimize':
            $('.socialflow-batch-post-form .form-item-must-send').show();
            $('.socialflow-batch-post-form .form-item-date-and-time').show();
            $('.socialflow-batch-post-form .form-item-end-date').show();
            $('.socialflow-batch-post-form .form-item-date-and-time label[for="edit-date-and-time"]').text('Optimize from');
          break;

          case 'publish':
            $('.socialflow-batch-post-form .form-item-must-send').hide();
            $('.socialflow-batch-post-form .form-item-date-and-time').hide();
            $('.socialflow-batch-post-form .form-item-end-date').hide();
          break;

          case 'schedule':
            $('.socialflow-batch-post-form .form-item-must-send').hide();
            $('.socialflow-batch-post-form .form-item-date-and-time').show();
            $('.socialflow-batch-post-form .form-item-end-date').hide();
            $('.socialflow-batch-post-form .form-item-date-and-time label[for="edit-date-and-time"]').text('Send at');
          break;

          case 'hold':
            $('.socialflow-batch-post-form .form-item-must-send').hide();
            $('.socialflow-batch-post-form .form-item-date-and-time').show();
            $('.socialflow-batch-post-form .form-item-end-date').hide();
            $('.socialflow-batch-post-form .form-item-date-and-time label[for="edit-date-and-time"]').text('Expires at');
          break;
        }
      });

      $('.socialflow-post-form textarea').keydown(function(event_data) {
        SOCIALFLOW.postFormHelper.textCounter($(this), event_data);
      });
      $('.socialflow-post-form textarea').focusout(function(eventObject){
        if ($(this).val().length > 140) {
          text = $(this).val();
          new_text = text.substring(0,140);
          $(this).val(new_text);
        }
      });

      $('.node-form .form-item-socialflow-message textarea').keydown(function(event_data) {
        SOCIALFLOW.postFormHelper.textCounter($(this), event_data, 118);
      });
      $('.node-form .form-item-socialflow-message textarea').focusout(function(eventObject){
        if ($(this).val().length > 118) {
          text = $(this).val();
          new_text = text.substring(0,118);
          $(this).val(new_text);
        }
      });

      $('.socialflow-batch-post-form textarea').keydown(function(event_data) {
        SOCIALFLOW.postFormHelper.textCounter($(this), event_data, 118);
      });
      $('.socialflow-batch-post-form textarea').focusout(function(eventObject){
        if ($(this).val().length > 118) {
          text = $(this).val();
          new_text = text.substring(0,118);
          $(this).val(new_text);
        }
      });
    },
    textCounter:function(textarea, event_data, maxlimit) {
      if(!maxlimit)
        maxlimit = 140;
      length = (textarea.val().length);
      value = textarea.val();
      if (event_data.keyCode == 8) {
        backspace_length = maxlimit-(length-1);
        if(backspace_length == (maxlimit + 1)) {
          backspace_length = maxlimit;
        }
        $('.socialflow-character-limit').text(backspace_length);
      } else {
        if ((length + 1) > maxlimit)
          textarea.val(value.substring(0, maxlimit));
        else
          $('.socialflow-character-limit').text(maxlimit-(length+1));
      }
    },
    nodeFormTextCounter:function(textarea, event_data) {
      maxlimit = 118;
      length = (textarea.val().length);
      value = textarea.val();
      if (event_data.keyCode == 8) {
        backspace_length = maxlimit-(length-1);
        if(backspace_length == (maxlimit + 1)) {
          backspace_length = maxlimit;
        }
        $('.socialflow-character-limit').text(backspace_length);
      } else {
        if ((length + 1) > maxlimit)
          textarea.val(value.substring(0, maxlimit));
        else
          $('.socialflow-character-limit').text(maxlimit-(length));
      }
    }
  };

  SOCIALFLOW.datePickerHelper = {
    init:function(){
      $.datepicker.setDefaults(
        {
          onSelect:function(){
            $(this).parent().next().find('input').val('12:00 am');
          } 
        }
      );
    }
  };

  SOCIALFLOW.messageHelper = {
    init:function() {
      $('.form-item-title input').keyup(function(event_data){
        $('#edit-socialflow-message').text($(this).val());
        SOCIALFLOW.postFormHelper.nodeFormTextCounter($('#edit-socialflow-message'), event_data);
      });
    }
  };

  SOCIALFLOW.contentQueueNavigation = {
    init:function(){
      $('#edit-socialflow-accounts').change(function(event_data) {
        window.location = 'http://' + location.host + '/admin/config/socialflow/' + event_data.srcElement.value;
      });
    }
  };

  SOCIALFLOW.validationHelper = {
    init:function(status){
      $(document).ready(function(){
        $('#edit-status-' + status).click();
      });
    }
  };

  $(document).ready(function(){
    SOCIALFLOW.postFormHelper.init();
  });

})(jQuery);

<?php

/**
 * @file
 * This file contains the definition for the SocialflowOAuth class.
 * This is the mediator between the Drupal module and SocialFlow.
 */

/**
 * SocialflowOAuth class
 * This class acts as the intermediary between the drupal module and SocialFlow
 */
class SocialflowOAuth {

  protected $signature_method;

  protected $consumer;

  protected $token;

  protected $host = 'www.socialflow.com';

  protected $api_host = 'api.socialflow.com';

  protected $username;

  protected $password;

  /**
   * Constructor
   */
  public function __construct($consumer_key, $consumer_secret, $oauth_token = NULL, $oauth_token_secret = NULL) {
    $this->signature_method = new OAuthSignatureMethod_HMAC_SHA1();
    $this->consumer = new OAuthConsumer($consumer_key, $consumer_secret);
    if (!empty($oauth_token) && !empty($oauth_token_secret)) {
      $this->token = new OAuthConsumer($oauth_token, $oauth_token_secret);
    }
  }

  /**
   * getRequestToken
   */
  public function getRequestToken(&$params = array()) {
    $url = $this->createUrl('oauth/request_token');
    try {
      if (!empty($params['destination'])) {
        $destination = str_replace('/', '^~', $params['destination']);
        $response = $this->authRequest($url, array(), 'GET', $destination);
      }
      else {
        $response = $this->authRequest($url);
      }

      parse_str($response, $token);
      $this->token = new OAuthConsumer($token['oauth_token'], $token['oauth_token_secret']);
      $_SESSION['socialflow_request_token_sec'] = $token['oauth_token_secret'];
      return $token;
    }
    catch (Exception $e) {
      return $e;
    }

  }

  /**
   * setToken
   */
  public function setToken($token) {
    $this->token = $token;
  }

  /**
   * getAuthorizeUrl
   */
  public function getAuthorizeUrl($token) {
    $url = $this->createUrl('oauth/authorize', '');
    $url .= '?oauth_token=' . $token['oauth_token'];

    return $url;
  }

  /**
   * getAuthenticateUrl
   */
  public function getAuthenticateUrl($token) {
    $url = $this->createUrl('oauth/authenticate', '');
    $url .= '?oauth_token=' . $token['oauth_token'];

    return $url;
  }

  /**
   * getAccessToken
   */
  public function getAccessToken($params) {
    $url = $this->createUrl('oauth/access_token', '');
    try {
      $response = $this->authRequest($url, $params, 'GET');
      parse_str($response, $token);
      variable_set('socialflow_oauth_token', $token['oauth_token']);
      variable_set('socialflow_oauth_token_secret', $token['oauth_token_secret']);
      $this->token = new OAuthConsumer($token['oauth_token'], $token['oauth_token_secret']);
      return $token;
    }
    catch (Exception $e) {
      print_r($e);die();
      return $e;
    }
  }

  /**
   * authorize
   */
  public function authorize($token) {
    try {
      $url = $this->getAuthorizeUrl($token);
        drupal_goto($url);
    } catch (Exception $e) {
      return $e;
    }
  }

  /**
   * getAccounts
   */
  public function getAccounts() {
    $url = $this->createApiUrl('account/list');
    try {
      $response = $this->authRequest($url, array(), 'GET');
      $json_response = json_decode($response);
      return $json_response->data->client_services;
    } catch (Exception $e) {
      return $e;
    }
  }

  /**
   * getContentQueue
   */
  public function getContentQueue($social_account,  $account_type, $page = 1, $limit = 20) {
    $url = $this->createApiUrl('contentqueue/list');
    try {
      $params = array();
      $params['service_user_id'] = $social_account;
      $params['account_type'] = $account_type;
      // $params['page'] = $page;
      // $params['limit'] = $limit;
      $response = $this->authRequest($url, $params, 'GET');
      $json_response = json_decode($response);
      return $json_response->data;
    } catch (Exception $e) {
      return $e;
    }
  }

  /**
   * getFeeds
   */
  public function getFeeds($service_user_id, $account_type) {
    $url = $this->createApiUrl('feed/list');
    try {
      $params = array();
      $params['service_user_id'] = $service_user_id;
      $params['account_type'] = $account_type;

      $response = $this->authRequest($url, $params, 'GET');
      $json_response = json_decode($response);
      return $json_response->data;
    }
    catch (Exception $e) {
      return $e;
    }
  }

  /**
   * addFeed
   */
  public function addFeed($service_user_id, $account_type, $feed_url, $feed_name) {
    $url = $this->createApiUrl('feed/add');
    try {
      $params = array();
      $params['service_user_id'] = $service_user_id;
      $params['account_type'] = $account_type;
      $params['feed_url'] = rawurlencode($feed_url);
      $params['name'] = $feed_name;

      $response = $this->authRequest($url, $params, 'POST');
      $json_response = json_decode($response);
      return $json_response->data;
    }
    catch (Exception $e) {

    }
  }

  /**
   * addMessageToQueue
   */
  public function addMessageToQueue($service_user_id, $account_type, $message, $publish_option, $start_date = NULL, $end_date = NULL, $must_send = 0, $expiration_date = NULL, $shorten_links = 1) {
    $url = $this->createApiUrl('message/add');
    try {
      $params = array();
      $params['service_user_id'] = $service_user_id;
      $params['account_type'] = $account_type;
      $params['publish_option'] = $publish_option;
      $params['message'] = $message;
      $params['shorten_links'] = $shorten_links;

      switch ($publish_option) {
        case 'optimize':
          if (!empty($start_date)) {
            $params['must_send'] = $must_send;
            $params['optimize_start_date'] = $start_date;
            $params['optimize_end_date'] = $end_date;
          }
          break;

        case 'schedule':
          $params['scheduled_date'] = $start_date;
          break;

        case 'hold':
          $params['expiration_date'] = $expiration_date;
          break;
      }

      $response = $this->authRequest($url, $params, 'POST');
      $json_response = json_decode($response);
      return $json_response->data;
    } catch (Exception $e) {
      return $e;
    }
  }

  /**
   * deleteMessageFromQueue
   */
  public function deleteMessageFromQueue($content_item_id, $service_user_id, $account_type) {
    $url = $this->createApiUrl('message/delete');
    try {
      $params = array();
      $params['content_item_id'] = $content_item_id;
      $params['service_user_id'] = $service_user_id;
      $params['account_type'] = $account_type;

      $response = $this->authRequest($url, $params, 'POST');
      $json_response = json_decode($response);
      return $json_response->data;
    } catch (Exception $e) {
      return $e;
    }
  }

  /**
   * editMessage
   */
  public function editMessage($service_user_ids, $content_item_id, $account_type, $message, $publish_option, $start_date = NULL, $end_date = NULL, $expiration_date = NULL, $shorten_links = 1) {
    $url = $this->createApiUrl('message/edit');
    try {
      $params['service_user_id'] = $service_user_ids;
      $params['content_item_id'] = $content_item_id;
      $params['account_type'] = $account_type;
      $params['publish_option'] = $publish_option;
      $params['message'] = urldecode($message);
      $params['shorten_links'] = $shorten_links;

      switch ($publish_option) {
        case 'optimize':
          if (!empty($start_date)) {
            $params['optimize_start_date'] = $start_date;
            $params['optimize_end_date'] = $end_date;
          }
          break;

        case 'schedule':
          $params['scheduled_date'] = $start_date;
          break;

        case 'hold':
          $params['expiration_date'] = $expiration_date;
          break;
      }

      $response = $this->authRequest($url, $params, 'POST');
      $json_response = json_decode($response);
      return $json_response->data;

    } catch (Exception $e) {
      return $e;
    }
  }

  /**
   * shortenLinks
   */
  public function shortenLinks($account_type, $message) {
    $url = $this->createApiUrl('link/shorten_message');
    try {
      $params = array();
      $params['account_type'] = $account_type;
      $params['message'] = $message;

      $response = $this->authRequest($url, $params, 'GET');
      $json_response = json_decode($response);
      return $json_response->data;
    } catch (Exception $e) {
      return $e;
    }
  }

  /**
   * authRequest
   */
  protected function authRequest($url, $params = array(), $method = 'GET', $destination = '') {
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $this->token, $method, $url, $params);
    global $base_url;
    if (!empty($destination)) {
      $request->set_parameter('oauth_callback', $base_url . '/socialflow/authorize_callback/' . $destination . '/');
    }

    $request->sign_request($this->signature_method, $this->consumer, $this->token);

    switch ($method) {
      case 'GET':
        return $this->request($request->get_normalized_http_url(), $request->get_parameters());
      case 'POST':
        return $this->request($request->get_normalized_http_url(), $request->get_parameters(), 'POST');
    }
  }

  /**
   * request
   */
  protected function request($url, $params = array(), $method = 'GET') {
    $data = '';

    if (count($params) > 0) {
        $url .= '?' . http_build_query($params, '', '&');
    }

    $headers = array();

    $response = drupal_http_request($url, array('headers' => $headers, 'method' => $method, 'data' => $data));

    if (!isset($response->error)) {
      return $response->data;
    }
    else {
      $error = $response->error;
      throw new Exception($error);
    }
  }

  /**
   * createUrl
   */
  protected function createUrl($path = '') {
    $url = 'https://' . $this->host . '/' . $path;
    return $url;
  }

  /**
   * createApiUrl
   */
  protected function createApiUrl($path = '') {
    $url = 'https://' . $this->api_host . '/' . $path;
    return $url;
  }

  /**
   * parseResponse
   */
  protected function parseResponse($response, $format = NULL) {
    if (empty($format)) {
      $format = $this->format;
    }

    switch ($format) {
      case 'json':
        // http://drupal.org/node/985544 - json_decode large integer issue.
        $length = drupal_strlen(PHP_INT_MAX);
        $response = preg_replace('/"(id|in_reply_to_status_id)":(\d{' . $length . ',})/', '"\1":"\2"', $response);
        return json_decode($response, TRUE);
    }
  }
}

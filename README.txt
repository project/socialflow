SOCIALFLOW INTEGRATION

ABOUT
=======================
SocialFlow is a social media optimization platform that helps organizations
increase their audience engagement on various social networks. The company
uses the Twitter firehose, access to click data from bitly, metrics from
Facebook and proprietary algorithms to optimize the delivery of messages on
social networks. The company believes that understanding and utilizing key
metrics of engagement, such as clicks per Tweet and clicks per Follower, is
central to growing a large and active social media following.

Sign up for an account at: http://www.socialflow.com/

INSTALLATION
=======================
Install the module just like you would any other.  You will then be prompted to
sign in to SocialFlow.  You must sign in to SocialFlow in order for the module
to work correctly.  Enter your correct SocialFlow credentials and you should be
redirected back to your site with the proper access keys to make authenticated
calls to the SocialFlow API.

SETTINGS
=======================
Go to Configuration > SocialFlow > Settings (admin/config/socialflow).
From here you can choose which content types you would like to associate
SocialFlow with. You can choose one or all.  From that point forward, you will
notice a SocialFlow form on the node/add and node/edit pages of that
content type.

CONTENT QUEUE
=======================
You can use this module to administer the content queues associated with your
SocialFlow account.  Go to Configuration > SocialFlow > Content Queue
(admin/config/socialflow/content_queue)

ACCOUNTS
=======================
View which social accounts are connected to your SocialFlow account by going
to Configuration > SocialFlow > Accounts (admin/config/socialflow/accounts)

CONTENT SOURCES
========================
Go to Configuration > SocialFlow > Content Sources
(admin/config/socialflow/feeds). Click "Add Content Source." Add RSS or Atom
Feed URL, feed title and select account to send content to. Then click submit.

SEND MULTIPLE POSTS TO SOCIALFLOW
========================
Go to Configuration > SocialFlow > Send Multiple to SocialFlow
(admin/config/socialflow/batch) to send more than one piece of content at once.

DASHBOARD BLOCK
========================
If you utilize the drupal 7 dashboard the module provides a "Send to
SocialFlow" dashboard block that can be placed in any region. Configure the
dashboard here: (admin/dashboard/configure)

FAQ
=======================
Visit the FAQ here:
http://support.socialflow.com/entries/20568202-drupal-module-faq-help
